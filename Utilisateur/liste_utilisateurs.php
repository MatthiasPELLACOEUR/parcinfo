<?php
require '../nav_bar.php';

$req = $bdd->prepare('SELECT * FROM user');
$req->execute();
$users = $req->fetchAll(PDO::FETCH_ASSOC);

?>

        <div class="container">
            <h3>Liste des utilisateurs</h3>
            <table class="table">
                <thead>
                    <tr>
                        <th class="col-md-3">Prénom</th>
                        <th class="col-md-3">Nom</th>
                        <th class="col-md-3">Pays</th>
                        <th style="text-align:end;">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($users as $user) :?>
                    <tr>
                        <td class="col-md-2"><?php echo $user["prenom"]?></td>
                        <td class="col-md-2"><?php echo $user["nom"]?></td>
                        <td class="col-md-5"><?php echo $user["pays"]?></td>
                        <td class="col-md-2" style="text-align:end;">
                            <a href="./modifier_utilisateur.php<?php echo '?idUser=' . $user["id"]; ?>"><i class="bi bi-pencil-fill"></i></a>
                            <a href="./supprimer_utilisateur.php<?php echo '?idUser=' . $user["id"]; ?>"><i class="bi bi-trash3-fill"></i></a>
                        </td>
                    </tr>
                    <?php endforeach;?>
                </tbody>
            </table>
        </div>
        
    </body>
</html>