<?php
require '../connexion_bdd.php';

$url = "https://data.gouv.nc/api/records/1.0/search/?dataset=liste-des-pays-et-territoires-etrangers&q=&rows=300&sort=libcog&facet=libcog";

$json_data = file_get_contents($url);
$country_data = json_decode($json_data, false);

$result = "";
$erreur = "";
$success = "";

if(isset($_POST["submit"]) && isset($_POST["prenom"]) && isset($_POST["nom"]) && (!empty($_POST["pays"]) || $_POST["pays"] != "Selectionnez un pays"))
{
    $prenom = $_POST["prenom"];
    $nom = $_POST["nom"];
    $pays = $_POST["pays"];
    $nom_formated;

    if(strpos($nom, " "))
    {
        $b_login = true;
        $nom_formated = str_replace(" ", "", $nom);
        $login = strtolower($prenom[0]) . strtolower($nom_formated);
    }
    else if (strpos($nom, "-"))
    {
        $b_login = true;
        $nom_formated = str_replace("-", "", $nom);
        $login = strtolower($prenom[0]) . strtolower($nom_formated);
    }
    else if (strpos($nom, "_"))
    {
        $b_login = false;
    }
    else 
    {
        $b_login = true;
        $nom_formated = $nom;   
        $login = strtolower($prenom[0]) . strtolower($nom_formated);
    }

    if($b_login)
    {
        $reqLogin = $bdd->prepare('SELECT * FROM user WHERE login = ?');
        $reqLogin->execute(array($login));
        $loginExist = $reqLogin->rowCount();
        if(strlen($prenom) <= 255)
        {
            if(strlen($nom) <= 254)
            {
                if(strlen($login) <= 255 && $loginExist == 0)
                {
                    $insertUser = $bdd->prepare('INSERT INTO user(prenom, nom, login, pays) VALUES (?, ?, ?, ?)');
                    $success = $insertUser->execute(array($prenom, $nom, $login, $pays));
                    if($success)
                    {
                        $loginReq = $bdd->prepare('SELECT id, login FROM user WHERE login = ?');
                        $loginReq->execute(array($login));
                        $result = $loginReq->fetchAll(PDO::FETCH_ASSOC);

                        if(!empty($result))
                        {
                            $_SESSION["user"] = $result[0]['id'];
                            $_SESSION["login"] = $result[0]['login'];

                            header('Location: ./liste_utilisateurs.php');
                        }
                        else
                        {
                            echo 'chut';
                        }
                    }
                    else
                    {
                        $erreur = 'Une erreur est survenue lors de la création de l\'utilisateur';
                    }
                }
                else 
                {
                    $erreur = "Login trop long ou déjà existant.";
                }
            }
            else
            {
                $erreur = "Nom trop long.";
            }
        }
        else 
        {
            $erreur = "Prenom trop long.";
        }
    }
    else 
    {
        $erreur = "Le caractère _ n'est pas autorisée dans le nom de famille.";
    }
}
else if(isset($_POST["submit"]) && isset($_POST["prenom"]) && !isset($_POST["nom"]))
{
   $erreur = "Vous devez saisir votre nom.";
}
else if(isset($_POST["submit"]) && !isset($_POST["prenom"]) && isset($_POST["nom"]))
{
   $erreur = "Vous devez saisir votre prenom.";
}
else if(isset($_POST["submit"]) && isset($_POST["prenom"]) && isset($_POST["nom"]) && $_POST["pays"] == "Selectionnez un pays")
{
    $erreur = "Renseignez un pays.";
}
else if(isset($_POST["submit"]) && !isset($_POST["prenom"]) && !isset($_POST["nom"]))
{
   $erreur = "Tous les champs doivent être complétés.";
}

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <title>Inscription</title>
    </head>
    <body>
    <div class="container">
            <h3>Inscription</h3>
            <form method="POST" action="" class="form-inline">
                <div class="form-group mx-2 mb-2">
                    <label for="exampleInputEmail1" class="sr-only">Prénom</label>
                    <input type="text" class="form-control" name="prenom" placeholder="Prénom">
                </div>
                <div class="form-group mx-2 mb-2">
                    <label for="exampleInputEmail1" class="sr-only">Nom</label>
                    <input type="text" class="form-control" name="nom" placeholder="Nom">
                </div>
                <select name="pays" class="custom-select custom-select-sm mx-2">
                    <option value="">Selectionnez un pays</option>
                <?php foreach($country_data->records as $country) : ?>
                    <option value="<?php echo $country->fields->libcog; ?>"><?php echo $country->fields->libcog; ?></option>
                <?php endforeach; ?>
                </select>
                <button type="submit" name="submit" class="btn btn-primary mb-2">S'inscrire</button>
            </form>
            <a href="./connexion_user.php">Déjà un compte ?</a>

            <?php
            
                    if(!empty($erreur)){
                        echo '<br/><font color="red">'.$erreur.'</font>';
                    }
                    else if($success){
                        echo '<br/><font color="green">Le compte a bien été créé.</font>';
                    }
                    if($result != ""){
                        echo '<br/>Votre Identifiant est ' . $result[0]["login"];
                    }
            ?>

        </div>
    </body>
</html>