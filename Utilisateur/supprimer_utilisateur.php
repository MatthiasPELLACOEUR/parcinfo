<?php
require '../nav_bar.php';

$supprBesoin = $bdd->prepare('DELETE FROM besoins WHERE id_user = ?');
$supprBesoin->execute(array($_GET["idUser"]));

$supprBesoin = $bdd->prepare('DELETE FROM user WHERE id = ?');
$supprBesoin->execute(array($_GET["idUser"]));

if($_GET["idUser"] == $_SESSION["user"])
{
    header('Location: ../Utilisateur/deconnexion.php');
}
else 
{
    header('Location: ./liste_utilisateurs.php');
}
