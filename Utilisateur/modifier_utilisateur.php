<?php
require '../connexion_bdd.php';
require '../nav_bar.php';

$result = "";
$url = "https://data.gouv.nc/api/records/1.0/search/?dataset=liste-des-pays-et-territoires-etrangers&q=&rows=300&sort=libcog&facet=libcog";

$json_data = file_get_contents($url);
$response_data = json_decode($json_data, false);
$country_data = $response_data;
$result = "";
$erreur="";


$loginReq = $bdd->prepare('SELECT * FROM user WHERE id = ?');
$loginReq->execute(array($_GET['idUser']));
$result = $loginReq->fetchAll(PDO::FETCH_ASSOC);
$user = $result[0];

if(isset($_POST["submit"]) && isset($_POST["prenom"]) && isset($_POST["nom"]) && !empty($_POST["pays"]))
{
    $prenom = strtolower($_POST["prenom"]);
    $nom = strtolower($_POST["nom"]);
    $login = $prenom[0] . $nom;
    $pays = $_POST["pays"];

    $reqLogin = $bdd->prepare('SELECT * FROM user WHERE login = ?');
    $reqLogin->execute(array($login));
    $loginExist = $reqLogin->rowCount();

    if(strlen($prenom) <= 255)
    {
        if(strlen($nom) <= 254)
        {
            if(strlen($login) <= 255 && $loginExist == 0)
            {
                $insertUser = $bdd->prepare('UPDATE user SET prenom = ?, nom = ?, login = ?, pays = ? WHERE id = ?');
                $insertUser->execute(array($prenom, $nom, $login, $pays, $_GET['idUser']));

                $loginReq = $bdd->prepare('SELECT id, login FROM user WHERE id = ?');
                $loginReq->execute(array($_GET['idUser']));
                $result = $loginReq->fetchAll(PDO::FETCH_ASSOC);
                
                $_SESSION["user"] = $result[0]['id'];
                $_SESSION["login"] = $result[0]['login'];
                header('Location: ./liste_utilisateurs.php');
            }
            else {
                $erreur = "Login trop long ou déjà existant.";
            }
        }else{
            $erreur = "Nom trop long.";
        }
    }else {
        $erreur = "Prenom trop long.";
    }
}
else if(isset($_POST["submit"]) && isset($_POST["prenom"]) && !isset($_POST["nom"]))
{
   $erreur = "Vous devez saisir votre nom.";
}
else if(isset($_POST["submit"]) && !isset($_POST["prenom"]) && isset($_POST["nom"]))
{
   $erreur = "Vous devez saisir votre prenom.";
}
else if(isset($_POST["submit"]) && !isset($_POST["prenom"]) && !isset($_POST["nom"]))
{
   $erreur = "Tous les champs doivent être complétés.";
}

?>

    <div class="container">
            <h3>Inscription</h3>
            <form method="POST" action="" class="form-inline">
                <div class="form-group mx-2 mb-2">
                    <label for="exampleInputEmail1" class="sr-only">Prénom</label>
                    <input type="text" class="form-control" name="prenom" value="<?php echo $user["prenom"]?>">
                </div>
                <div class="form-group mx-2 mb-2">
                    <label for="exampleInputEmail1" class="sr-only">Nom</label>
                    <input type="text" class="form-control" name="nom" value="<?php echo $user["nom"]?>">
                </div>
                <select name="pays" class="custom-select custom-select-sm mx-2">
                    <option value="<?php echo $user["pays"]; ?>"><?php echo $user["pays"]; ?></option>
                    <option value="">---------------------</option>
                    <?php foreach($country_data->records as $country) : ?>
                        <option value="<?php echo $country->fields->libcog; ?>"><?php echo $country->fields->libcog; ?></option>
                    <?php endforeach; ?>
                </select>
                <button type="submit" name="submit" class="btn btn-primary mb-2">Modifier</button>
            </form>
            <a href="./liste_utilisateurs.php">Annuler ?</a>

            <?php
            
                    if(!empty($erreur)){
                        echo '<br/><font color="red">'.$erreur.'</font>';
                    }
            ?>

        </div>
    </body>
</html>