<?php 
require '../connexion_bdd.php';

$result = "";

if(isset($_POST["submit"]) && isset($_POST["identifiant"]))
{
    $identifiant = strtolower($_POST["identifiant"]);
    $req = $bdd->prepare('SELECT * FROM user WHERE login = ?');
    $req->execute(array($identifiant));
    $result = $req->fetchAll(PDO::FETCH_ASSOC);
    if($result)
    {
        $_SESSION["user"] = $result[0]['id'];
        $_SESSION["login"] = $result[0]['login'];
        header('Location: ./liste_utilisateurs.php');
    }
    else
    {
        header('Location: ./inscription.php');
    }
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <title>Connexion</title>
    </head>
    <body>
        <div class="container">
            <h3>Connexion</h3>
            <form method="POST" action="" class="form-inline">
                <div class="form-group mx-2 mb-2">
                    <label for="exampleInputEmail1" class="sr-only">Identifiant</label>
                    <input type="text" class="form-control" name="identifiant" placeholder="Identifiant">
                </div>
                <button type="submit" name="submit" class="btn btn-primary mb-2">Se connecter</button>
            </form>
            <a href="./inscription.php">Pas de compte ?</a>
        </div>
    </body>
</html>