<?php
require '../nav_bar.php';

$req = $bdd->prepare('SELECT * FROM besoins INNER JOIN user ON besoins.id_user = user.id');
$req->execute();
$besoins = $req->fetchAll(PDO::FETCH_ASSOC);

function getDateFromString( $str )
{
    $date = DateTime::createFromFormat( 'd.m.y', $str );
    if ( $date !== false )
        return $date->getTimestamp();
    return strtotime( $str );
}

?>

        <div class="container">
            <h3>Liste des utilisateurs</h3>
            <table class="table">
                <thead>
                    <tr>
                        <th>Utilisateur</th>
                        <th style="text-align:center;">À changer</th>
                        <th style="text-align:center;">Souhaité</th>
                        <th style="text-align:center;">Date souhaité</th>
                        <th style="text-align:end;">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($besoins as $besoin) :?>
                    <tr>
                        <td><?php echo $besoin["login"]?></td>
                        <td style="text-align:center;"><?php echo $besoin["materiel_change"]?></td>
                        <td style="text-align:center;"><?php echo $besoin["materiel_voulu"]?></td>
                        <td style="text-align:center;"><?php echo date('d/m/Y', getDateFromString($besoin["date_voulu"]))?></td>
                        <td style="text-align:end;">
                            <a href="./modifier_besoin.php<?php echo '?id_besoin=' . $besoin["id_besoin"]; ?>"><i class="bi bi-pencil-fill"></i></a>
                            <a href="./supprimer_besoin.php<?php echo '?id_besoin=' . $besoin["id_besoin"]; ?>"><i class="bi bi-trash3-fill"></i></a>
                        </td>
                    </tr>
                    <?php endforeach;?>
                </tbody>
            </table>
        </div>
        
    </body>
</html>