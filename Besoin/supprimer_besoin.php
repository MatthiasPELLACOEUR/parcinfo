<?php
require '../connexion_bdd.php';
require '../nav_bar.php';


$supprBesoin = $bdd->prepare('DELETE FROM besoins WHERE id_besoin = ?');
$supprBesoin->execute(array($_GET["id_besoin"]));

header('Location: ./liste_besoins.php');
