<?php 
require '../nav_bar.php';

$req = $bdd->prepare('SELECT * FROM besoins INNER JOIN user ON besoins.id_user = user.id WHERE besoins.id_besoin = ?');
$req->execute(array($_GET['id_besoin']));
$besoins = $req->fetchAll(PDO::FETCH_ASSOC);
$besoin = $besoins[0];

date_default_timezone_set("Europe/Paris");
if(isset($_POST["submit"]) && isset($_POST["date_voulu"]) && isset($_POST["materiel_change"]) && !empty($_POST["materiel_voulu"])) 
{
    if($_POST["date_voulu"] > date('Y-m-d'))
    {
        $date_voulu = $_POST["date_voulu"];
        // var_dump(date('Y-m-d'));
        $materiel_change = $_POST["materiel_change"];
        $materiel_voulu = $_POST["materiel_voulu"];
        // exit(0);
        if(isset($_POST["commentaire"]))
        {
            $commentaire = $_POST["commentaire"];
        }
        else
        {
            $commentaire = null;
        }

        if(strlen($materiel_change) <= 255 && strlen($materiel_voulu) <= 255)
        {
            $reqCreaBesoin = $bdd->prepare('UPDATE besoins SET materiel_change = ?, materiel_voulu = ?, date_voulu = ?, commentaire = ? WHERE id_besoin = ?');
            $reqCreaBesoin->execute(array($materiel_change, $materiel_voulu, $date_voulu, $commentaire, $_GET['id_besoin']));
            header('Location: ./liste_besoins.php');
        }
        else
        {
            $message = 'Le nom du matériel a changé et celui voulu ne doivent pas dépassé 255 caracteres.';
        }
    }
    else
    {
        $message = 'La date selectionné doit être au minimum demain.';
    }
}
else
{
    $message = "Remplissez tous les champs nécessaire (*).";
}
?>

        <div class="container">
            <h3>Soumettre un besoin</h3>
            <form method="POST" action="">
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="" class="sr-only">Matériel à remplacer (*)</label>
                        <input type="text" class="form-control" name="materiel_change" value="<?php echo $besoin["materiel_change"]; ?>">
                    </div>
                    (*)
                    <div class="form-group col-md-4">
                        <label for="" class="sr-only">Matériel souhaité (*)</label>
                        <input type="text" class="form-control" name="materiel_voulu" value="<?php echo $besoin["materiel_voulu"]; ?>">
                    </div>
                    (*)
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label class="sr-only">Commentaire</label>
                        <input type="text-area" name="commentaire" class="form-control" placeholder="Commentaire (facultatif)">
                    </div>
                    <div class="form-group col-md-2">
                        <label for="" class="sr-only">Date souhaitée</label>
                        <input type="date" id="date" class="form-control" name="date_voulu" min="<?php echo date('Y-m-d'); ?>">
                    </div>
                    (*)
                </div>
                <button type="submit" name="submit" class="btn btn-primary mb-2">Soumettre</button>
            </form>
            <p style="color:red;"><?php echo $message;?></p>
        </div>
    </body>
</html>
    
