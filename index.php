<?php
require './connexion_bdd.php';

if(!isset($_SESSION["user"]))
{
    header('Location: ./Utilisateur/connexion_user.php');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <title>Gestion de Parc</title>
</head>
<body>
    <div class="container">
        <h3>Gestion de Parc</h3>
        <ul class="list-group list-group-flush">
            <li class="list-group-item"><a href="./Utilisateur/liste_utilisateurs.php">Liste utilisateurs</a></li>
            <li class="list-group-item"><a href="./Besoin/liste_besoins.php">Liste besoins</a></li>
            <li class="list-group-item"><a href="./Besoin/creation_besoin.php">Soumettre besoin</a></li>
        </ul>
    </div>

</body>
</html>
