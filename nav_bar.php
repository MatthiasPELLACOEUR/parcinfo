<?php
require 'connexion_bdd.php';

$page = basename($_SERVER["PHP_SELF"]);
switch($page)
{
    case 'liste_utilisateurs.php':
        $title = 'Liste utilisateurs';
        break;

    case 'connexion_user.php':
        $title = 'Connexion';
        break;

    case 'inscription.php':
        $title = 'Inscription';
        break;

    case 'modifier_utilisateur.php':
      $title = 'Modifier utilisateur';
      break; 

    case 'creation_besoin.php':
        $title = 'Soumission besoin';
        break;
    
    case 'liste_besoins.php':
        $title = 'Liste besoins';
        break;
    
    case 'modifier_besoin.php':
        $title = 'Modifier besoin';
        break; 
}
$user = (!isset($_SESSION["login"])) ? 'Connexion' : $_SESSION["login"];
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.3/font/bootstrap-icons.css">
    <title><?php echo $title ?></title>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="../index.php">Gestion de Parc</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarText">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="../Utilisateur/liste_utilisateurs.php">Liste Utilisateurs</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="../Besoin/liste_besoins.php">Liste besoins</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="../Besoin/creation_besoin.php">Soumission besoin</a>
      </li>
    </ul>
    <span class="navbar-text">
        <?php if($user == 'Connexion'){echo '<a href="../Utilisateur/connexion_user.php">' . $user .'</a>';}else{echo $user;} ?>      
    </span>
    <span class="navbar-text">
        <a href="<?php if($user != 'Connexion'){echo '../Utilisateur/deconnexion.php';}?>"><i class="bi bi-power"></i></a>
    </span>
  </div>
</nav>