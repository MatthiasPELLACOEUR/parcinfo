# Besoin de créer la base de données `parc_informatique` avant de lancer le script SQL

### Possible améliorations :
    - ajouter une date quand le matériel a bien été changé;
    - enlever de la liste des besoins ce qui ont été validé et les mettre dans une liste "archive"
    - bouton de changement effectué qui met à jour directement la BDD avec la date du jour et 
        passe un booléen de 0 a 1 concernant le changement ou vérification fait avec la date 
        (si il y en a une alors ce besoin fait parti des archives)
    - rajouter un système d'administration au minimum pour éviter que tout le monde puisse 
        supprimer/modifier les comptes des autres et/ou leurs besoins
    - rajouter la validation de suppression
    - étendre les besoins a autre chose que du matériel informatique 
        par ex : papier, cahier, stylo, etc. Par extension, créer des catégories.

